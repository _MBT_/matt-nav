# Matt-Nav

A set of software modules built to be used for GPS independent position estimation. The idea is that these run on a payload that interfaces with the GPS receiver connection point of your autopilot. This software is built for Linux, it will not work on WIndows. Only executables are provided. See Wiki for complete documentation.